/********************************************************************************************\
	File Name:      SkunkCast.js
	Purpose:        Functions for spell casting.
	Creator:        Cyprias
	Date:           04/27/2015
	License:        MIT License	(http://opensource.org/licenses/MIT)
\*********************************************************************************************/

var MAJOR = "SkunkCast-1.0";
var MINOR = 200323;

(function (factory) {
    if (typeof module === 'object' && module.exports) {
        module.exports = factory();
	} else {
        SkunkCast10 = factory();
	}
}(function () {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}

	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	core.Error = Error;
	core.setInterval = setInterval;
	core.setTimeout = setTimeout;
	core.setImmediate = setImmediate;
	core.clearTimeout = clearTimeout;

	var AnimationFilter     = (typeof LibStub !== "undefined" && LibStub.getLibrary("AnimationFilter-1.0", true)) || (typeof require === "function" && require("SkunkCast\\AnimationFilter"));
	var EffectsFilter     = (typeof LibStub !== "undefined" && LibStub.getLibrary("EffectsFilter-1.0", true)) || (typeof require === "function" && require("SkunkCast\\EffectsFilter"));

	core.debugging = false;
	
	var castSpellTimeout = 5000;// ~2.8s for level 7 self spell. Some extra for spinning around to face target.
	
	var spellDiffs = {};// Difficulty to level.
	spellDiffs[1]       = 1;
	spellDiffs[15]      = 1;
	spellDiffs[25]      = 1;
	spellDiffs[38]      = 1; //Drain Health Other I (38)
	spellDiffs[50]      = 2;
	spellDiffs[75]      = 2;
	spellDiffs[88]      = 2;//Drain Health Other II (88)
	spellDiffs[100]     = 3;
	spellDiffs[125]     = 3;
	spellDiffs[138]     = 3; //Drain Health Other III (138)
	spellDiffs[150]     = 4;
	spellDiffs[175]     = 4;
	spellDiffs[188]     = 4;//Drain Health Other IV (188)
	spellDiffs[200]     = 5;
	spellDiffs[225]     = 5;
	spellDiffs[238]     = 5;//Drain Health Other V (238)
	spellDiffs[250]     = 6;
	spellDiffs[275]     = 6;
	spellDiffs[285]     = 6;//Unnatural Persistence (285) (diamond spell)
	spellDiffs[288]     = 6;//Drain Health Other VI (288)
	spellDiffs[300]     = 7;
	spellDiffs[325]     = 7;//Bafflement Self VII (325)
	spellDiffs[350]     = 7;//Thaumaturgic Shroud (350)
	spellDiffs[375]     = 7;// Nullify All Magic Other (375)
	spellDiffs[400]     = 8;	

	var radarRadius = 75 / 240; // We can't cast on objects past radar range. The client will fail with no feedback. We'll add outOfRadarRange if target is beyond radius.

	core.console = function consoleLog() { // 'console' is taken by SkunkWorks.
		skapi.OutputLine("[SkunkCast] " + Array.prototype.slice.call(arguments).join('\t'), opmConsole);
	};

	/*
		debug: Print a message to chat.
	*/
	core.debug = function debug() {
		if (core.debugging == true) {
			console.apply(core, arguments);
		}
	};

	/*
		getHeadingTurn: Return the angle from our current heading needed to face the intended heading. Positive number = turn right, negitive number = turn left.
	*/
	function getHeadingTurn(intendedHeading, currentHeading) {
		if (typeof currentHeading === "undefined") currentHeading = skapi.maplocCur.head;
		var heading = ((intendedHeading - currentHeading) % 360);
		while (heading < 0) {
			heading += 360; // % in jscript doesn't touch negative numbers...
		}
		if (heading > 180) {
			heading -= 360;
		}
		return heading;
	}
	
	function findCaster(spawn_maploc, heading) {
		var acf = skapi.AcfNew();
		acf.maploc = spawn_maploc;
		acf.ocm = ocmPlayer | ocmMonster;
		acf.distMax = 0.02; //0.01 for single projectiles, 0.02 for multi projectiles (walls), 0.006 for rings.
		var coaco = acf.CoacoGetSorted(skapi.sortbyDist);
		
		//core.debug(core.MAJOR + " coaco: " + coaco.Count);
	
		var aco;
		var dist;
		var diff;
		
		var probableAco;
		var probableDiff = 360; // 10 on retail
		for (var i = 0; i < coaco.Count; i++) {
			aco = coaco.Item(i);
			dist = spawn_maploc.Dist3DToMaploc(aco.maploc);
			//diff = Math.abs(heading - aco.maploc.head);
			diff = Math.abs(getHeadingTurn(heading, aco.maploc.head));
			//core.debug(core.MAJOR + " Closest: " + aco + ", dist: " + dist + ", diff: " + diff);
			if (diff < probableDiff) {
				probableAco = aco;
				probableDiff = diff;
			}
		}
		
	//	core.debug(core.MAJOR + " probableDiff: " + probableDiff);
		return probableAco;
	};
	
	core.applyMethod = function applyMethod(method, thisArg, args) {
		return method.apply(thisArg, args);
	};
	
	////////////////////////
	core.castSpell = function() {

		return function castSpell(params, callback) {
			var executeStart = new Date().getTime();

			var watchForStart = (!params || params.watchForStart === undefined ? true : params.watchForStart);
			var spellid = params.spellid;
			var onlyWatchStart =  (!params || params.watchForStart === undefined ? false : params.watchForStart);

			function resolve(/*args...*/) {
				if (handler) skapi.RemoveHandler(evidNil, handler);
				if (AnimationFilter) {
					AnimationFilter.removeCallback(onAnimation);
				}
				if (EffectsFilter) {
					EffectsFilter.removeCallback(onEffect);
				}
				core.clearTimeout(startedTimer);
				core.clearTimeout(tid);
				core.setImmediate(core.applyMethod, callback, params && params.thisArg, arguments);
			}
			
			
			// Make sure we're in world.
			if (skapi.plig != pligInWorld) {
				return resolve(new core.Error("NOT_IN_WORLD"));
			}

			if (typeof params.acoTarget === "undefined") {
				params.acoTarget = skapi.acoChar;
			}

			if (params.acoTarget && !params.acoTarget.fExists) {
				return resolve(new core.Error("INVALID_TARGET"));
			}
			var oidTarget = params.acoTarget.oid;
			
			var spell = skapi.SpellInfoFromSpellid(spellid);
			if (!spell) {
				core.debug("Could not get spell object for " + spellid);
				return resolve(new core.Error("INVALID_SPELLID"));
			}

			var tid = setTimeout(core.timedOut, castSpellTimeout, resolve, "CAST_SPELL_TIMED_OUT");
			
			// Timer that goes off in one second, if we see a wind up animation we cancel this timer.
			//if (core.isHarmfulSpell(spell)) {
			var startedTimer;
			if (watchForStart == true && params.acoTarget.oid != skapi.acoChar.oid) {
				startedTimer = core.setTimeout(core.timedOut, 1000, resolve, "START_CAST_TIMED_OUT");
			}

			if (AnimationFilter) {
				AnimationFilter.addCallback(onAnimation);
			}
			function onAnimation(payload) {
				if (!skapi.acoChar || payload.object != skapi.acoChar.oid) return;
				
				/**/
				if (payload.animation_1 == 306) {// lvl 8, 7
					core.debug(core.MAJOR + " Level 8/7 spell animation");
					if (onlyWatchStart == true) return resolve(undefined, true);
					return core.clearTimeout(startedTimer);
				} else if (payload.animation_1 == 120) {// lvl 6
					core.debug(core.MAJOR + " Level 6 spell animation");
					if (onlyWatchStart == true) return resolve(undefined, true);
					return core.clearTimeout(startedTimer);
				} else if (payload.animation_1 == 118) {// lvl 5
					core.debug(core.MAJOR + " Level 5 spell animation");
					if (onlyWatchStart == true) return resolve(undefined, true);
					return core.clearTimeout(startedTimer);
				} else if (payload.animation_1 == 116) {// lvl 4
					core.debug(core.MAJOR + " Level 4 spell animation");
					if (onlyWatchStart == true) return resolve(undefined, true);
					return core.clearTimeout(startedTimer);
				} else if (payload.animation_1 == 114) {// lvl 3
					core.debug(core.MAJOR + " Level 3 spell animation");
					if (onlyWatchStart == true) return resolve(undefined, true);
					return core.clearTimeout(startedTimer);
				} else if (payload.animation_1 == 112) {// lvl 2
					core.debug(core.MAJOR + " Level 2 spell animation");
					if (onlyWatchStart == true) return resolve(undefined, true);
					return core.clearTimeout(startedTimer);
				}
				
				
				//if (payload.animation_1 == 44) { // Lifestone recall bubbles?
				//	core.debug(core.MAJOR + " Lifestone recall bubbles");
				//	return resolve(undefined, true);
				//}
				
				//if (payload.stance == payload.stance2) {
				//	return;
				//}
				
				if (payload.animation_type == 8) {
					// Turning to object.
					return;
				}
				
				
				
				if (payload.animation_1 == 47) {
					// Protection other
					return;
				} else if (payload.animation_1 == 57) {
					// Protection self
					return;
				}

				if (payload.animation_1 == 51) {
					// Launch bolt animation.
					return resolve(undefined, true);
				}
				


				//core.debug("<castSpell|onAnimation> UNCAUGHT " + JSON.stringify(payload));

				//var thisArg = this;
				//var executeStart = thisArg.executeStart;
				//var params = thisArg.params;
				//var startedTimer = thisArg.startedTimer;
				//var resolve = thisArg.resolve;
				
				
				/*
				var startedCast = false;
				
				if (payload.animation_1 == 306) {// lvl 8, 7
					core.debug(core.MAJOR + " Level 8/7 spell animation");
					startedCast = true;
				} else if (payload.animation_1 == 120) {// lvl 6
					core.debug(core.MAJOR + " Level 6 spell animation");
					startedCast = true;
				} else if (payload.animation_1 == 118) {// lvl 5
					core.debug(core.MAJOR + " Level 5 spell animation");
					startedCast = true;
				} else if (payload.animation_1 == 116) {// lvl 4
					core.debug(core.MAJOR + " Level 4 spell animation");
					startedCast = true;
				} else if (payload.animation_1 == 114) {// lvl 3
					core.debug(core.MAJOR + " Level 3 spell animation");
					startedCast = true;
				} else if (payload.animation_1 == 112) {// lvl 2
					core.debug(core.MAJOR + " Level 2 spell animation");
					startedCast = true;
				//} else if (payload.animation_1 == 51) { // lvl 1 launch (no winde up). This can confuse us if our previous spell isn't done.
				//	startedCast = true;
				//} else if (payload.animation_1) {
				//} else if (payload.animation_1 == 55) {
				//	fConsole("We cast ths spell?");
				
				//} else if (payload.activity == 0 && payload.stance == 73 && payload.flags == 1) {
				//	fConsole("Fizzle animation?");
				//	//fConsole("<OnAnimation> " + JSON.stringify(payload, null, "\t"));
				//	return resolve(new core.Error("SPELL_FIZZLED"));
				//return;
				} else if (payload.animation_type == 8 && payload.target == params.acoTarget.oid) {
					core.debug(core.MAJOR + " We're turning to cast something...");
					startedCast = true;
				//} else {
				//	fConsole("<OnAnimation> " + JSON.stringify(payload, null, "\t"));
				}

				if (startedCast == true) {
					var executeElasped = new Date().getTime() - executeStart;
					core.debug(core.MAJOR + " Started casting: " + executeElasped);
					core.clearTimeout(startedTimer);
			//		core.debug(core.MAJOR + " Registering action complete...");
				//	skapi.AddHandler(evidOnActionComplete,    handler);
					
					if (params.onlyWatchStart == true) {
						return resolve(null, true);
					}
				}
				*/
			}
			
			if (EffectsFilter) {
				EffectsFilter.addCallback(onEffect);
			}
			function onEffect(payload) {
				//fConsole("<onEffect> " + JSON.stringify(payload, null, "\t"));
				if (!skapi.acoChar || payload.object != skapi.acoChar.oid) return;
				if (payload.effect == 16) { // purple bubbles around us, happens during summoning spell.
					core.debug(core.MAJOR + " Portal recall bubbles");
					return resolve(null, true);
				} else if (payload.effect == 81) {
					core.debug(core.MAJOR + " Fizzle effect?");
					return resolve(new core.Error("SPELL_FIZZLED"));
				} else if (payload.effect == 116) {
					core.debug(core.MAJOR + " Lifestone recall bubbles");
					return resolve(undefined, true);
				}
				
				core.debug("<castSpell|onEffect> UNCAUGHT " + JSON.stringify(payload, null, "\t"));
			}
			
			
			// If spell is item magic, watch for pruple bubbles summon spell effect.
			//if (spell && (spell.skid == skidItemEnchantment)) {
			//	EffectsFilter.addCallback(onEffect);
			//}
			
			// Events
			var handler = {};
			handler.OnChatServer = function OnChatServer(szMsg, cmc) {
				
				if (szMsg.match(/You cast (.*) on (.*), refreshing (.*)/i)) {
					var spellName = RegExp.$1;
					core.debug("We cast " + spellName, spell.szName, spellName == spell.szName);
					if (spellName == spell.szName) {
						return resolve(null, true);
					}
				} else if (szMsg.match(/You cast (.*) on (.*), but it is surpassed by (.*)/i)) {
					var spellName = RegExp.$1;
					core.debug("We cast " + spellName, spell.szName, spellName == spell.szName);
					if (spellName == spell.szName) {
						return resolve(null, true);
					}
				} else if (szMsg.match(/^You cast (.*) on/)) {
					//core.debug(core.MAJOR + " <fCastSpell><OnChatServer>  '" + szMsg + "'");
					var spellName = RegExp.$1;
					core.debug("We cast " + spellName, spell.szName, spellName == spell.szName);
					if (spellName == spell.szName) {
						return resolve(null, true);
					}
				} else if (szMsg.match(/(.*) resists your spell/)) {
					// Only check for resists on non-projectile type spells. Otherwise if we launch a bolt and start casting another this will catch the previous bolt's resist message.
					if (!spell || (spell.skid != skidWarMagic && spell.skid != skidVoidMagic)) {
						core.debug(core.MAJOR + " <fCastSpell><OnChatServer>  '" + szMsg + "'");
						return resolve(new core.Error("SPELL_RESISTED"));
					}
				} else if (szMsg.match(/The spell consumed the following components: (.*)/)){
					return;
				} else if (szMsg.match(/(.*) has defeated (.*)!/)){
					return;
				} else if (szMsg.match(/With (.*) you restore (\d*) points of (\w*) to (.*)./)) {
					//core.debug(core.MAJOR + " <fCastSpell><OnChatServer>  '" + szMsg + "'");
					return resolve(null, true);
				} else if (szMsg.match(/Target is out of range!/)) {
					//core.debug(core.MAJOR + " <fCastSpell><OnChatServer>  '" + szMsg + "'");
					return resolve(new core.Error("SPELL_OUT_OF_RANGE"));
				} else if (szMsg.match(/You lose (\d*) points of (\w*) due to casting (.*) on (.*)/)) {
					//core.debug(core.MAJOR + " <fCastSpell><OnChatServer>  '" + szMsg + "'");
					return resolve(null, true);
				} else if (szMsg.match(/Your movement disrupted spell casting!/)) {
					//core.debug(core.MAJOR + " <fCastSpell><OnChatServer>  '" + szMsg + "'");
					return resolve(new core.Error("MOVEMENT_DISRUPTED"));
				} else {
					core.debug(core.MAJOR + " <fCastSpell><OnChatServer> UNCAUGHT szMsg: '" + szMsg + "'");
				}
			};
			skapi.AddHandler(evidOnChatServer, handler);
			
			handler.OnTipMessage = function OnTipMessage(szMsg) {
				// Watch for Moved too far message.
				if (szMsg.match(/You're too busy/)) {
					//core.debug(core.MAJOR + " <fCastSpell><OnTipMessage>  '" + szMsg + "'");
					return resolve(new core.Error("TOO_BUSY"));
				} else if (szMsg.match(/You do not have all of this spell's components/)) {
					//core.debug(core.MAJOR + " <fCastSpell><OnTipMessage>  '" + szMsg + "'");
					return resolve(new core.Error("MISSING_SPELL_COMPONENTS"));
				} else if (szMsg.match(/You don't know that spell!/)) {
					//core.debug(core.MAJOR + " <fCastSpell><OnTipMessage>  '" + szMsg + "'");
					return resolve(new core.Error("DONT_KNOW_SPELL"));
				} else if (szMsg.match(/You don't have enough Mana to cast this spell./)) {
					//core.debug(core.MAJOR + " <fCastSpell><OnTipMessage>  '" + szMsg + "'");
					//core.debug(core.MAJOR + " <fCastSpell><OnTipMessage> skapi.manaCur: " + skapi.manaCur);
					return resolve(new core.Error("NOT_ENOUGH_MANA"));
				} else if (szMsg.match(/That item doesn't have enough Mana./)) {
					return;
				} else if (szMsg.match(/Casting (.*)/)) {
					return;
				} else if (szMsg.match(/You are unprepared to cast a spell/)) {
					//core.debug(core.MAJOR + " <fCastSpell><OnTipMessage>  '" + szMsg + "'");
					// jumping/falling.
					return resolve(new core.Error("UNPREPARED"));
				} else if (szMsg.match(/In Portal Space - Please Wait.../))  {
					//core.debug(core.MAJOR + " <fCastSpell><OnTipMessage>  '" + szMsg + "'");
					// Entered a portal.
					return resolve(new core.Error("IN_PORTAL_SPACE"));
				} else if (szMsg.match(/Your projectile spell mislaunched!/)) {
					//core.debug(core.MAJOR + " <fCastSpell><OnTipMessage>  '" + szMsg + "'");
					return resolve(new core.Error("PROJECTILE_MISLAUNCHED"));
				} else if (szMsg.match(/Unable to move to object!/)) {
					//core.debug(core.MAJOR + " <fCastSpell><OnTipMessage> " + szMsg);
					return resolve(new core.Error("UNABLE_MOVE_OBJECT"));
				} else if (szMsg.match(/Out of Range!/)) { // Emulator
					//core.debug(core.MAJOR + " <fCastSpell><OnChatServer>  '" + szMsg + "'");
					return resolve(new core.Error("SPELL_OUT_OF_RANGE"));
				} else {
					core.debug(core.MAJOR + " <fCastSpell><OnTipMessage> UNCAUGHT szMsg: '" + szMsg + "'");
				}
			};
			skapi.AddHandler(evidOnTipMessage, handler);
			
			handler.OnChatBoxMessage = function OnChatBoxMessage(szMsg, cmc) {
				if (szMsg.match(/^You have been involved in a player killer battle too recently to do that!/)) {
					//core.debug(core.MAJOR + " <fCastSpell><OnChatBoxMessage>  '" + szMsg + "'");
					return resolve(new core.Error("PK_RESTRICTED"));
				} else if (cmc == 7 && szMsg.match(/Your spell fizzled./)) { // Sometimes doesn't fire on PhatAC 
					//core.debug(core.MAJOR + " <fCastSpell><OnChatBoxMessage>  '" + szMsg + "'");
					return resolve(new core.Error("SPELL_FIZZLED"));
				} else if (cmc == 7 && szMsg.match(/You fail to affect (.*) because you are acting across a house boundary!/)) {
					//core.debug(core.MAJOR + " <fCastSpell><OnChatBoxMessage>  '" + szMsg + "'");
					return resolve(new core.Error("HOUSE_BOUNDARY"));
				} else if (szMsg.match(/You fail to summon the portal!/)) {
					//core.debug(core.MAJOR + " <fCastSpell><OnChatServer>  '" + szMsg + "' (" + cmc + ")");
					return resolve(new core.Error("FAILED_SUMMON_PORTAL"));
				} else if (szMsg.match(/You must have linked with a portal in order to recall to it!/)) {
					//core.debug(core.MAJOR + " <fCastSpell><OnChatServer>  '" + szMsg + "' (" + cmc + ")");
					return resolve(new core.Error("NO_LINKED_PORTAL"));
				} else if (szMsg.match(/You say, "(.*)"/)) {
					return;
				} else {
					core.debug(core.MAJOR + " <fCastSpell><OnChatBoxMessage> UNCAUGHT szMsg: '" + szMsg + "' cmc: " + cmc);
				}
			};
			skapi.AddHandler(evidOnChatBoxMessage, handler); 
			
			handler.OnObjectDestroy = function OnObjectDestroy(aco) {
				if (aco.oid == oidTarget) {
					core.debug(core.MAJOR + " <CastSpell|OnObjectDestroy> Target destoryed mid cast (" + aco.szName + ")");
					return resolve(new core.Error("INVALID_TARGET"));
				}
			};
			skapi.AddHandler(evidOnObjectDestroy, handler); 
			
			if (spell && (spell.skid == skidWarMagic || spell.skid == skidVoidMagic)) {
				handler.OnObjectCreate = function OnObjectCreate(aco) {
					if (aco.spellid == spellid) {
						core.debug(core.MAJOR + " Object created matching our spellid", spellid);
						//spawn_maploc, heading
						var acoCaster = findCaster(aco.maploc, aco.maploc.head);
						core.debug(core.MAJOR + " acoCaster: " + acoCaster);
						if (acoCaster && acoCaster == skapi.acoChar) {
							return resolve(null, true);
						}
					}
				};
				skapi.AddHandler(evidOnObjectCreate,    handler); 
			}

			core.debug(core.MAJOR + " Calling skapi.CastSpell(" + spell.szName + "," + params.acoTarget + ")...");
			skapi.CastSpell(spellid, params.acoTarget);	
		};
	}();

	/*
		getSpellComps: Return the components needed to cast a spell.
	*/
	core.getSpellComps = function getSpellComps(spellid) {
		var comps = {};
		
		var spell = skapi.SpellInfoFromSpellid(spellid);

		/*
			// Lets just assume we have the foci, don't want to deal with augs right now.
		if (spell.skid == skidItemEnchantment) {
			comps.push("Foci of Artifice");
		} else if (spell.skid == skidCreatureEnchantment) {
			comps.push("Foci of Enchantment");
		} else if (spell.skid == skidWarMagic) {
			comps.push("Foci of Strife");
		} else if (spell.skid == skidLifeMagic) {
			comps.push("Foci of Verdancy");
		} else if (spell.skid == skidVoidMagic) {
			comps.push("Foci of Shadow");
		}
		*/
		
		if (spell.diff <= 25) {
			comps["Lead Scarab"] = 1;
			comps["Prismatic Taper"] = 1;
		} else if (spell.diff <= 75) {
			comps["Iron Scarab"] = 1;
			comps["Prismatic Taper"] = 2;
		} else if (spell.diff <= 125) {
			comps["Copper Scarab"] = 1;
			comps["Prismatic Taper"] = 3;
		} else if (spell.diff <= 175) {
			comps["Silver Scarab"] = 1;
			comps["Prismatic Taper"] = 3;
		} else if (spell.diff <= 225) {
			comps["Gold Scarab"] = 1;
			comps["Prismatic Taper"] = 4;
		} else if (spell.diff <= 275) {
			comps["Pyreal Scarab"] = 1;
			comps["Prismatic Taper"] = 4;
		} else if (spell.diff <= 325) {
			comps["Platinum Scarab"] = 1;
			comps["Prismatic Taper"] = 4;
		} else if (spell.diff <= 400) {
			comps["Mana Scarab"] = 1;
			comps["Prismatic Taper"] = 4;
		}
		return comps;
	};
	
	/*
		hasSpellComps: Return true/false whether we have the components to cast a spell.
	*/
	core.hasSpellComps = function hasSpellComps(spellid) {
		//core.debug(core.MAJOR + " <hasSpellComps> " + spellid);
		var comps = core.getSpellComps(spellid);
		
		var desc = skapi.SzDescFromSpellid(spellid);
		if (desc) {
			if (desc.match(/wall/) || desc.match(/outward/)) {
				comps["Diamond Scarab"] = 2;
			}
		}
/*
		for (var szComp in comps) {
			if (!comps.hasOwnProperty(szComp)) continue;
			core.debug(core.MAJOR + " We need " + szComp + "*" + comps[szComp]);
		}
*/	
		var acf = skapi.AcfNew();
		acf.mcm = mcmSpellComponents;
		//acf.olc = olcInventory; // Some reason just olcInventory is returning main pack.

		var coaco = acf.CoacoGet();
		
		core.debug(core.MAJOR + " coaco: " + coaco.Count);
		var aco;
		for (var i = 0; i < coaco.Count; i++) {
			aco = coaco.Item(i);
			if (!(aco.olc & olcInventory)) continue;
			
			//core.debug(core.MAJOR + " We have " + aco.szName + " (" + aco.citemStack + ") at " + aco.olc);
			if (comps[aco.szName]) {
				comps[aco.szName] -= aco.citemStack;
			}
		}

		for (var szComp in comps) {
			if (!comps.hasOwnProperty(szComp)) continue;

			if (comps[szComp] >= 1) {
				core.debug(core.MAJOR + " We don't have enough " + szComp);
				return false;
			}
		}
		
		return true;
	};
	
	var _romanNumerals= ["", "I", "II", "III", "IV", "V", "VI", "VII"]; //8s are done separately
	var knownSpellIds = {}; // Some spellids have the same name which confuses skapi.spellidFromSz(), so we check for known spells before using spellidFromSz();
	knownSpellIds["Clouded Soul"]                                 = 5361;// skapi.spellidFromSz() reports spell as 5331.
		
	core.spellidFromSz = function spellidFromSz(szName) {
		// Some spells have same names, so search our known spells before checking skapi.spellidFromSz(). 
		if (typeof knownSpellIds[szName] !== "undefined") return knownSpellIds[szName];
		core.debug(core.MAJOR + " <spellidFromSz> " + szName);
		/*
		// We've already cached known spells in bootstrapKnownSpellIds().
		var spell;
		for (var i = 0; i < skapi.spellidMax; i++) {
			spell = skapi.SpellInfoFromSpellid(i);
			if (!spell) continue;
			if (spell.szName != szName) continue;
			
			if (skapi.FSpellidInSpellbook(spell.spellid)) {
				knownSpellIds[szName] = spell.spellid;
				return spell.spellid;
			}
		}
		*/
		knownSpellIds[szName] = skapi.spellidFromSz(szName) || 0;
		return knownSpellIds[szName];
	};
	
	// Fill the knownSpellIds table on startup for quicker lookups.
	function bootstrapKnownSpellIds() {
		core.debug(core.MAJOR + " <bootstrapKnownSpellIds>");
		var spell;
		for (var i = 0; i < skapi.spellidMax; i++) {
			spell = skapi.SpellInfoFromSpellid(i);
			if (!spell) continue;
			
			if (skapi.FSpellidInSpellbook(spell.spellid) || typeof knownSpellIds[spell.szName] === "undefined") {
				knownSpellIds[spell.szName] = spell.spellid;
			}
		}
		core.debug(core.MAJOR + " </bootstrapKnownSpellIds>");
	}
	bootstrapKnownSpellIds();
	
	var commonSpellIds = {};
	core.getCommonSpellsIds = function getCommonSpellsIds(szCommonName) {
		core.debug(core.MAJOR + " <getCommonSpellsIds> " + szCommonName);
		if (typeof commonSpellIds[szCommonName] !== "undefined") {
			return commonSpellIds[szCommonName];
		}

		var szSelfless;
		if (szCommonName.match(/(.*) Self/)) { // Arcanum Salvaging VII doesn't have 'Self' in the name. =/
			szSelfless = RegExp.$1;
		}
		
		var spellIds = [];
		
		var fullName;
		var spellid;
		for (var i = 0; i < _romanNumerals.length; i++) {
			fullName = szCommonName;
			if (_romanNumerals[i] != "") fullName += " " + _romanNumerals[i];
			spellid = core.spellidFromSz(fullName);
			
			core.debug(i + " " + fullName + " " + spellid);
			
			if (spellid) {
				spellIds.push(spellid);
			} else if (szSelfless) {
				fullName = szSelfless;
				if (_romanNumerals[i] != "") fullName += " " + _romanNumerals[i];
				spellid = core.spellidFromSz(fullName);
				if (spellid) spellIds.push(spellid);
			}
		}
		
		if (szCommonName.match(/Aura of (.*) Self/)) {
			// Aura of Spirit Drinker > Aura of Incantation of Spirit Drinker
			// Aura of Blood Drinker Self > Aura of Incantation of Blood Drinker Self
			var szFull = "Aura of Incantation of " + RegExp.$1;
			var spellid = core.spellidFromSz(szFull);
			if (spellid) {
				core.debug(core.MAJOR + " 1: We have " + szFull +  "!");
				spellIds.push(spellid);
			}
			
			// Try self.
			szFull += " Self";
			var spellid = core.spellidFromSz(szFull);
			if (spellid) {
				core.debug(core.MAJOR + " 2: We have " + szFull +  "!");
				spellIds.push(spellid);
			}
		} else {
			var spellid = core.spellidFromSz("Incantation of " + szCommonName);
			if (spellid) {
				core.debug(core.MAJOR + " 3: We have Incantation of " + szCommonName + "!");
				spellIds.push(spellid);
			}
		}

		commonSpellIds[szCommonName] = spellIds;
		return spellIds;
	};
	
	core.getSkidLvlCur = function getSkidLvlCur(skid) {
		var value = 0;
		var skill = skapi.SkinfoFromSkid(skid);
		if (skill) {
			value = skill.lvlCur;// Doesn't include augs (+5 points).
		}
		return value;// + 5; // Just assume we have Jack of all Trades aug. 
	};

	var knownSpellSkids = {};
	
	/*
		getSpellSkid: Return the skid of a spell name.
	*/
	core.getSpellSkid = function getSpellSkid(szSpell) {
		if (typeof knownSpellSkids[szSpell] !== "undefined") {
			return knownSpellSkids[szSpell];
		}
		
		var spellIds = core.getCommonSpellsIds(szSpell);
		var spell;
		for (var i = spellIds.length - 1; i >= 0; i--) {
			spell = skapi.SpellInfoFromSpellid(spellIds[i]);
			knownSpellSkids[szSpell] = spell && spell.skid;
			return spell && spell.skid;
		}
	};

	/*
		getCommonSpellId: Return the spell id of a common spell name and a specific spell level.
	*/
	core.getCommonSpellId = function getCommonSpellId(params) {
		var szCommonName = params.szCommonName;
		var spellLevel = params.spellLevel;

		var spellIds = core.getCommonSpellsIds(szCommonName);
		
		var spellid, spell;
		for (var i = spellIds.length - 1; i >= 0; i--) {
			spellid = spellIds[i];
			spell = skapi.SpellInfoFromSpellid(spellid);
			
			if (spellDiffs[spell.diff] == spellLevel) {
				return spellid;
			}
		}
	};
	
	/*
		getHighestKnownSpellid: Return the the highest known and castable spellId.
	*/
	core.getHighestKnownSpellid = function getHighestKnownSpellid(params) { //szCommonName, minLevel, maxLevel, manaCheck, skillOffset
		core.debug(core.MAJOR + " <getHighestKnownSpellid> " + params);
		var szCommonName;
		if (typeof params !== "object") { // Legacy arguments..
			szCommonName = params;
			params = {};
			params.szCommonName = szCommonName;
			params.minLevel = arguments[1];
			params.maxLevel = arguments[2];
			params.manaCheck = arguments[3];
			params.skillOffset = arguments[4];
		}
		var checkForComps = typeof params.checkForComps !== 'undefined' ? params.checkForComps : true;

		if (typeof params.manaCheck === "undefined") params.manaCheck = false;
		var spellIds = core.getCommonSpellsIds(params.szCommonName);
		core.debug(core.MAJOR + " Got " + spellIds.length + " spellids for " + params.szCommonName);
		
		var skinfoMC = skapi.SkinfoFromSkid(skidManaConversion);
		
		var spellid;
		var spell;
		var lvlCur;
		var dist;
		for (var i = spellIds.length - 1; i >= 0; i--) {
			spellid = spellIds[i];
			spell = skapi.SpellInfoFromSpellid(spellid);
			core.debug(core.MAJOR + " " + i + " " + spell.szName);
			
			// Check if we have enough mana if mana conversion isn't trained.
			if (params.manaCheck == true && skinfoMC.skts < sktsTrained) { // && spell.skid != skidItemEnchantment
				if (skapi.manaCur < spell.mana) {
					core.debug(core.MAJOR + " Skipping " + spell.szName + " because we don't have enough mana. (" + skapi.manaCur + "<" + spell.mana + ").");
					continue;
				}
			}

			if (spellDiffs[spell.diff] > params.maxLevel) {
				core.debug(core.MAJOR + " Skipping " + spell.szName + " for being higher than " + params.maxLevel);
				continue;
			}
			if (spellDiffs[spell.diff] < params.minLevel) {
				core.debug(core.MAJOR + " Skipping " + spell.szName + " for being lower than " + params.minLevel);
				continue;
			}
			if (!skapi.FSpellidInSpellbook(spellid)) {
				core.debug(core.MAJOR + " Skipping " + spell.szName + " (" + spellid + ") for not being in spellbook.");
				continue;
			}
			
			lvlCur = core.getSkidLvlCur(spell.skid); 
			if (params.skillOffset) {
				lvlCur += params.skillOffset;
			}
			
			//if (params.skillOffset) {
				core.debug(core.MAJOR + " lvlCur " + lvlCur + ", diff: " + spell.diff + ", name: " + spell.szName);
			//}
			if (lvlCur < spell.diff) {
				core.debug(core.MAJOR + " Our skill isn't high enough to cast " + params.szCommonName + " " + _romanNumerals[i] + " (" + lvlCur + "<" + spell.diff + ")");
				continue;
			}
			
			if (checkForComps == true && !core.hasSpellComps(spellid)) {
				core.debug(core.MAJOR + " We don't have enough comps for " + spell.szName);
				continue;
			}
			
			// maploc param purposely seperate from aco.maploc. With maploc we intend to do a range check, without we do not.
			if (params.maploc) {
				dist = skapi.maplocCur.Dist3DToMaploc(params.maploc);
				if (dist > spell.range) {
					core.debug(core.MAJOR + " " + (params.aco && params.aco.szName || params.maploc) + " is too far away (" + (dist * 240) + "m) for " + spell.szName);
					continue;
				}
			}
			
			core.debug(core.MAJOR + " Returning spellid: " + spellid + " (" + spell.szName + ")");
			return spellid;
		}
	};
	
	/*
		getLowestKnownSpellid: Return the the highest known and castable spellId.
	*/
	core.getLowestKnownSpellid = function getLowestKnownSpellid(params) { //szCommonName, minLevel, maxLevel
		core.debug(core.MAJOR + " <getLowestKnownSpellid> " + params);
	
		var szCommonName;
		if (typeof params !== "object") { // Legacy arguments..
			szCommonName = params;
			params = {};
			params.szCommonName = szCommonName;
			params.minLevel = arguments[1];
			params.maxLevel = arguments[2];
		}
		
		var checkForComps = typeof params.checkForComps !== 'undefined' ? params.checkForComps : true;
		
		var spellIds = core.getCommonSpellsIds(params.szCommonName);
		
		var spellid;
		var spell;
		var lvlCur;
		for (var i = 0; i < spellIds.length; i++) {
			spellid = spellIds[i];
			spell = skapi.SpellInfoFromSpellid(spellid);

			if (spellDiffs[spell.diff] > params.maxLevel) continue;
			if (spellDiffs[spell.diff] < params.minLevel) continue;
			if (!skapi.FSpellidInSpellbook(spellid)) continue;
			
			lvlCur = core.getSkidLvlCur(spell.skid);
			if (lvlCur < spell.diff) {
				core.debug(core.MAJOR + " Our skill isn't high enough to cast " + params.szCommonName + " " + _romanNumerals[i] + " (" + lvlCur + "<" + spell.diff + ")");
				continue;
			}
			
			if (checkForComps == true && !core.hasSpellComps(spellid)) {
				core.debug(core.MAJOR + " We don't have enough comps for " + spell.szName);
				continue;
			}
			
			core.debug(core.MAJOR + " Returning spellid: " + spellid + " (" + spell.szName + ")");
			return spellid;
		}
	};
	
	/*
		getHighestKnownSpell: Return the the highest known and castable spell.
	*/
	core.getHighestKnownSpell = function getHighestKnownSpell(params) {// szCommonName, minLevel, maxLevel, skillOffset
		core.debug(core.MAJOR + " <getHighestKnownSpell> " + params);
	
		var szCommonName;
		if (typeof params !== "object") { // Legacy arguments..
			szCommonName = params;
			params = {};
			params.szCommonName = szCommonName;
			params.minLevel = arguments[1];
			params.maxLevel = arguments[2];
			params.skillOffset = arguments[3];
		}
	
		return skapi.SpellInfoFromSpellid(core.getHighestKnownSpellid(params));
	};
	
	/*
		getLowestKnownSpell: Return the the highest known and castable spell.
	*/
	core.getLowestKnownSpell = function getLowestKnownSpell(params) { // szCommonName, minLevel, maxLevel
		core.debug(core.MAJOR + " <getLowestKnownSpell> " + params);
	
		var szCommonName;
		if (typeof params !== "object") { // Legacy arguments..
			szCommonName = params;
			params = {};
			params.szCommonName = szCommonName;
			params.minLevel = arguments[1];
			params.maxLevel = arguments[2];
		}
	
		return skapi.SpellInfoFromSpellid(core.getLowestKnownSpellid(params));
	};
	
	/*
		inHealRange: Return true/false whether we can cast a heal spell somewhere. 
		Heal/revitalize have unusual ranges. Outdoors you can cast them anywhere within 75 meters and it'll land. Indoors you can attempt to cast anywhere within 75 meters but anything beyond 50m will get too far message.
	*/
	core.inHealRange = function inHealRange(maploc) {
		if (skapi.maplocCur && maploc) {
			// Return true/false if we can heal/revite someone at a maploc.
			var dist = skapi.maplocCur.Dist3DToMaploc(maploc);
			var maxRadius = radarRadius;//80 meters for ourdoors.
			if 	(skapi.maplocCur.fIndoors) {
				maxRadius = 50 / 240;// 50 meters.
			}
			return dist < maxRadius;
		}
	};

	core.getSpellLevelFromDiff = function getSpellLevelFromDiff(diff) {
		return spellDiffs[diff];
	};
	
	var commonSpellNameCache = {};
	core.getCommonSpellName = function getCommonSpellName(spell) {
		// Return the spell name without the roman numerals. 
		
		if (commonSpellNameCache[spell.spellid]) {
			return commonSpellNameCache[spell.spellid];
		}
		
		var szName = spell.szName;
		if (szName.match(/(.*) (I|II|III|IV|V|VI|VII)$/)) {
			return RegExp.$1;
		}
		
		// Spell might be a level 7, find a lower spell in the same family to return.
		var cospell = skapi.CospellFromFamily(spell.family);
		//Debug("Spell " + spell.szName + ", " + spell.szDesc + ", cospell: " + cospell.Count);
		var ispell;
		for (var i = 0; i < cospell.Count; i++) {
			ispell = cospell.Item(i);
		//	Debug(i + " " + ispell.szName + ", " + ispell.szDesc);
			if (spell.szDesc.match(/caster/) && !ispell.szDesc.match(/caster/)) {
				continue;
			}
			if (spell.szDesc.match(/target/) && !ispell.szDesc.match(/target/)) {
				continue;
			}
			if (ispell.szName.match(/(.*) (I|II|III|IV|V|VI|VII)$/)) {
				var szCommonName = RegExp.$1;
				commonSpellNameCache[spell.spellid] = szCommonName;
				return szCommonName;
			}
		}
		return szName;
	};
	
	core.getCommonSpellNameLevel = function getCommonSpellNameLevel(spell) {
		var common = core.getCommonSpellName(spell);
		if (common) {
			return common + " " + spellDiffs[spell.diff];
		}
		return spell.szName;
	};
		
	core.isHarmfulSpell = function isHarmfulSpell(spell) {
		if (spell.szDesc.match(/Decreases the target/)) {
			return true;
		} else if (spell.szDesc.match(/Increases damage the target/)) {
			return true;
		} else if (spell.szDesc.match(/^Drains /) && !spell.szDesc.match(/gives/)) { //Infuse drains and gives to someone.
			return true;
		} else if (spell.szDesc.match(/^Decreased target's/)) {
			return true;
		} else if (spell.szDesc.match(/^Shoots/)) {
			return true;
		} else if (spell.szDesc.match(/^Sends/)) {
			return true;
		} else if (spell.szDesc.match(/^The target loses/)) {
			return true;
		} else if (spell.szDesc.match(/^The heal rating of the target is decreased/)) {
			return true;
		} else if (spell.szDesc.match(/^Decrease target's/)) {
			return true;
		}
		return false;
	};

	core.inRadarRadius = function inRadarRadius(maploc) {
		return skapi.maplocCur && skapi.maplocCur.Dist3DToMaploc(maploc) <= radarRadius;
	};

	core.timedOut = function timedOut(callback, name) {
		callback(new core.Error(name || "TIMED_OUT"));
	};

	return core;
}));

/*
	Todo: 'You must have linked with a portal in order to recall to it!'
	
	Usage: 
	var SkunkCast = LibStub("SkunkCast-1.0");
	SkunkCast.castSpell({
		spellid: skapi.spellidFromSz("Strength Other I"),
		acoTarget: skapi.acoSelected
	}, function(err, result) {
		consoleLog(err, result);
	});
*/
